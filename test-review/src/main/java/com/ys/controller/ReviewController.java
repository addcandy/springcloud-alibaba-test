package com.ys.controller;

import com.alibaba.fastjson.JSON;
import com.ys.entity.Article;
import com.ys.entity.Review;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import com.ys.service.ReviewService;

import java.util.Date;

/**
 * @auther 烁烁
 * @date 2021/4/19 3:00
 */

@RestController
@Slf4j
public class ReviewController {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private ReviewService reviewService;

    @Autowired
    private DiscoveryClient discoveryClient;

    @RequestMapping("/Review/{aid}")
    public Review review(@PathVariable("aid")Integer aid){
        log.info("查询{}文章",aid);

        ServiceInstance instance = discoveryClient.getInstances("service-article").get(0);

        Article article = restTemplate.getForObject("http://"+instance.getHost()+":"+instance.getPort()+"/Article/"+aid,Article.class);
        log.info("查询文章成功，内容为{}", JSON.toJSON(article));

        Review review = new Review();
        review.setUid(1);
        review.setContent("123");
        review.setAid(article.getId());
        review.setCreatTime(new Date());

        reviewService.creatReview(review);

        log.info("成功{}",JSON.toJSON(review));
        return review;
    }
//    @RequestMapping("/Review/{aid}")
//    public Review review(@PathVariable("aid")Integer aid){
//        log.info("查询{}文章",aid);
//
//        Article article = restTemplate.getForObject("http://127.0.0.1:8081/Article/"+aid,Article.class);
//        log.info("查询文章成功，内容为{}", JSON.toJSON(article));
//
//        Review review = new Review();
//        review.setUid(1);
//        review.setContent("123");
//        review.setAid(article.getId());
//        review.setCreatTime(new Date());
//
//        reviewService.creatReview(review);
//
//        log.info("成功{}",JSON.toJSON(review));
//        return review;
//    }

















}
