package com.ys.service.impl;

import com.ys.dao.ReviewDao;
import com.ys.entity.Review;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ys.service.ReviewService;

/**
 * @auther 烁烁
 * @date 2021/4/19 2:54
 */
@Service
public class ReviewServiceImpl implements ReviewService {

    @Autowired
    private ReviewDao reviewDao;

    @Override
    public void creatReview(Review review) {
        reviewDao.save(review);
    }


}
