package com.ys.dao;

import com.ys.entity.Review;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @auther 烁烁
 * @date 2021/4/19 2:55
 */
public interface ReviewDao extends JpaRepository<Review,Integer> {
}
