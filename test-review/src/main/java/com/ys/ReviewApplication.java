package com.ys;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/**
 * @auther 烁烁
 * @date 2021/4/19 2:44
 */
@SpringBootApplication
@EnableDiscoveryClient
public class ReviewApplication {
    public static void main(String[] args) {
        SpringApplication.run(ReviewApplication.class);
    }

    @Bean
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }
}
