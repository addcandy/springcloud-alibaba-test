package com.ys.service;

import com.ys.entity.Article;

public interface ArticleService {

    Article findById(Integer id);
}
