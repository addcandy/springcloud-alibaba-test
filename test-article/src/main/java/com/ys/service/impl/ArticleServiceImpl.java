package com.ys.service.impl;

import com.ys.dao.ArticleDao;
import com.ys.entity.Article;
import com.ys.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @auther 烁烁
 * @date 2021/4/19 2:54
 */
@Service
public class ArticleServiceImpl implements ArticleService {

    @Autowired
    private ArticleDao articleDao;

    @Override
    public Article findById(Integer id) {
        return articleDao.findById(id).get();
    }
}
