package com.ys.controller;

import com.alibaba.fastjson.JSON;
import com.ys.entity.Article;
import com.ys.service.ArticleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @auther 烁烁
 * @date 2021/4/19 3:00
 */

@RestController
@Slf4j
public class ArticleController {

    @Autowired
    private ArticleService articleService;

    @RequestMapping("/Article/{id}")
    public Article article(@PathVariable("id")Integer id){
        log.info("查询{}文章",id);
        Article article = articleService.findById(id);

        log.info("查询文章成功，内容为{}", JSON.toJSON(article));

        return article;
    }
}
