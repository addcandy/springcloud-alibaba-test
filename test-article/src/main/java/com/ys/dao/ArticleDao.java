package com.ys.dao;

import com.ys.entity.Article;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @auther 烁烁
 * @date 2021/4/19 2:55
 */
public interface ArticleDao extends JpaRepository<Article,Integer> {
}
