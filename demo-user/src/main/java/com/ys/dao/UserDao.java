package com.ys.dao;

import com.ys.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @auther 烁烁
 * @date 2021/4/19 2:55
 */

public interface UserDao extends JpaRepository<User,Integer> {
}
